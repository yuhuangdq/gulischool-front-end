# 模块介绍

该部分是谷粒学院的前台前端部分，用于客户的浏览、交互与支付。

# 项目简介

谷粒学院是一种B2C商业模式的在线教育项目，分为网页前台系统与后台管理系统。前台系统具有课程查看、讲师展示、微信支付、视频播放等功能，后台系统具有课程分类管理、权限管理、订单管理、统计分析等功能。

# 技术栈

>  后端技术: SpringBoot + SpringCloud + MyBatis-Plus + HttpClient + MySQL + Maven + EasyExcel + Nginx
>
>  前端技术: Vue.js + Node.js +Element-UI + NUXT + ECharts
>
>  项目构建和部署: jenkins + Maven + Git
>
>  第三方技术:  阿里云OSS，阿里云视频点播，阿里云短信服务，微信支付，微信扫描

# 项目整体结构

![在线教育项目架构图](https://images.gitee.com/uploads/images/2021/0221/131725_d119919e_8178659.png "在线教育架构图 (2).png")

# 项目微服务架构

```
onlineEducation_backend
├── common -- 通用模块，保存可共用的API
	 └── common_utils -- 存放一些工具类
     ├── service_base -- 存放一些配置类
     └── spring-security -- 和springsecurity相关的配置类和过滤器等
└── service-- 系统的模块集合
     ├── service_acl -- 权限模块
     ├── service_cms -- 内容管理模块，比如banner
     ├── service_edu -- 教育模块(核心模块)，包含课程、讲师、章节、小节、评论等重要接口
     ├── service_msm -- 消息模块，验证码的发送
     ├── service_order -- 订单模块
     ├── service_oss -- 对象存储模块
     ├── service_pay -- 支付模块
     ├── service_statistics -- 统计模块
     ├── service_ucenter -- 用户模块
     └── service_vod -- 视频点播模块
└── infrastruture 网关模块
     └── api-gateway -- 网关子模块
```


# 项目开发流程与知识点

## mybatis-plus的应用

### 知识点1：mybatis-plus-自动添加主键

### 知识点2：mybatis-plus-主键生成策略

### 知识点3：mybatis-plus-驼峰命名和自动填充

### 知识点4：mybatis-plus-乐观锁和悲观锁

### 知识点5：mybatis-plus-物理删除和逻辑删除

## 后台系统-讲师管理模块

### 知识点1：代码生成器

### 知识点2：讲师CRUD

### 知识点3：整合swagger

### 知识点4：统一数据格式返回

### 知识点5：mybatis-plus实现分页查询

### 知识点6：mybatis-plus实现多条件分页查询

### 知识点7：统一异常处理

### 知识点8：统一日志处理

## ES6语法应用和Vue语法应用

### 知识点1：ES6语法-let定义变量

### 知识点2：ES6语法-解构模板和声明对象

### 知识点3：ES6语法-拷贝和合并对象

### 知识点4：ES6语法-箭头函数

### 知识点5：vue-单向和双向数据绑定

### 知识点6：vue-生命周期

### 知识点7：vue-路由

### 知识点8：axios的基本使用

## 前端各技术讲解

### 知识点1：element-ui

### 知识点2：nodejs

### 知识点3：nodejs

### 知识点4：npm

### 知识点5：babel

### 知识点6：模块化

### 知识点7：webpack

## 讲师管理-前端

### 知识点1：讲师CRUD

## 阿里云OSS+讲师头像上传+easyexcel

### 知识点1：阿里云OSS

### 知识点2：讲师头像上传

### 知识点3：easyexcel基本使用

## 课程分类管理和课程管理

### 知识点1：课程分类管理-添加课程分类

### 知识点2：课程分类管理-课程分类列表

### 知识点3：课程管理-添加课程

## 课程管理之添加课程

### 知识点1：课程管理-添加课程之课程大纲

### 知识点2：课程管理-添加课程之课程发布

## 阿里云视频点播+课程管理之添加小节

### 知识点1：阿里云视频点播

### 知识点2：课程管理之添加小节-上传和删除视频

## springcloud+nacos+课程管理

### 知识点1：springcloud

### 知识点2：nacos注册中心

### 知识点3：课程管理-删除视频

## nuxt + 首页数据显示

### 知识点1：nuxt框架

### 知识点2：首页数据显示-banner

## JWT + 阿里云短信

### 知识点1：JWT介绍

### 知识点2：阿里云短信实现验证码

### 知识点3：登录和注册

## 微信扫描登录

### 知识点1：微信扫描开发

## 前台系统-讲师详情和课程详情

### 知识点1：讲师详情

### 知识点2：课程详情

### 知识点3：课程详情页面小节部分-整合阿里云视频

## 评论（个人开发）+订单+微信支付

### 知识点1：评论功能

### 知识点2：订单功能

### 知识点3：微信支付

## 后台系统-统计功能+图表显示

### 知识点1：后台系统的统计功能

### 知识点2：Echarts实现图表显示

## canal数据同步 + SpringCloud-GateWay

### 知识点1：canal实现数据同步

### 知识点2：SpringCloud-GateWay代替nginx

## 权限管理 + springsecurity + nacos配置中心 + 提交代码到码云

### 知识点1：权限管理

### 知识点2：springsecurity基本使用

### 知识点3：naocs配置中心

### 知识点4：讲项目上传到码云

## 项目部署

### 知识点1：Jenkins实现项目自动化部署

